/**
 * Input: số có 2 chữ số
 * n = 88
 * 
 * Tiến hành tính tổng 2 kí số của n
 * 
 * Xuất kết quả
 */

var n = 88;
var sohangchuc = n / 10;
var sohangdonvi = n % 10;
var result = sohangchuc + sohangdonvi;
console.log('result: ', result);
